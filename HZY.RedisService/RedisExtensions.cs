using System;

namespace HZY.RedisService
{
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using Newtonsoft.Json;

    /// <summary>
    /// Redis 扩展
    /// </summary>
    public static class RedisExtensions
    {
        /// <summary>
        /// json 配置
        /// </summary>
        /// <value></value>
        static JsonSerializerSettings jsonConfig
            => new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };

        /// <summary>
        /// 注册 Redis 服务
        /// </summary>
        /// <param name="service"></param>
        /// <param name="connectionString"></param>
        public static void RegisterRedisService(this IServiceCollection service, string connectionString)
            => service.AddSingleton(new RedisService(connectionString));

        /// <summary>
        /// 根据 key 获取数据
        /// </summary>
        /// <param name="redisService"></param>
        /// <param name="key"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task<T> FindByKeyAsync<T>(this RedisService redisService, string key)
        {
            var database = redisService.Database;

            var redisPackageContent = await database.StringGetAsync(key);

            var redisPackage = JsonConvert.DeserializeObject<RedisPackage<T>>(redisPackageContent, jsonConfig);

            return redisPackage.Body;
        }

        /// <summary>
        /// 根据 key 插入 或者 修改 数据
        /// </summary>
        /// <param name="redisService"></param>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="cacheTime"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static async Task AddOrUpdateByKeyAsync<T>(this RedisService redisService, string key, T data, DateTime? cacheTime = null)
        {
            var database = redisService.Database;

            var keyExists = await database.KeyExistsAsync(key);
            if (keyExists)
            {
                await database.KeyDeleteAsync(key);
            }

            var redisPackage = new RedisPackage<T>(data, cacheTime);

            var redisPackageContent = JsonConvert.SerializeObject(redisPackage, jsonConfig);

            await database.StringSetAsync(key, redisPackageContent, TimeSpan.FromSeconds(redisPackage.ExpirationTime));
        }

        /// <summary>
        /// 根据 key 查看是否存在
        /// </summary>
        /// <param name="redisService"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Task<bool> ExistsByKeyAsync(this RedisService redisService, string key)
        {
            var database = redisService.Database;

            return database.KeyExistsAsync(key);
        }

        /// <summary>
        /// 根据 key 移除数据
        /// </summary>
        /// <param name="redisService"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<bool> DeleteByKeyAsync(this RedisService redisService, string key)
        {
            var database = redisService.Database;

            var keyExists = await database.KeyExistsAsync(key);

            if (keyExists)
            {
                return await database.KeyDeleteAsync(key);
            }

            return true;
        }






    }
}